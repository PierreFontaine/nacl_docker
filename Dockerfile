FROM fedora:latest

RUN dnf install -y make
RUN dnf install -y gcc
RUN dnf install -y nacl-20110221-20.fc29.x86_64
RUN dnf install -y  nacl-devel-20110221-20.fc29.x86_64
RUN dnf install -y nacl-devel-20110221-20.fc29.x86_64
RUN dnf install -y nacl-gcc-4.4.3-24.20150504gitf80d6b9.fc29.x86_64

CMD ["/bin/bash"]
