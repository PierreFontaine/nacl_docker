# nacl_docker

## Issue tracker

If you find bug, report bug to (incoming+pierrefontaine-nacl-docker-11207088-issue-@incoming.gitlab.com)

## Pre requesite 

Obviously you may have Docker installed on your laptop

## Build image

This image is based on Fedora official image. 
It will install all dependencies that are usefull for a C application using
nacl librairies.

```bash
docker build -t fednacl .
```

## Check evrything is ok

```bash
$ docker image ls
REPOSITORY                            TAG                 IMAGE ID            CREATED             SIZE
fednacl                               latest              30ae503fdc1b        6 minutes ago       1.18GB
```

OOOPSY 1.18GB !!! Sorry we won't save the nature today. So nasty !

I'll learn to make lightweight container later :)

## Run your Docker with your Environment

First move to your projet location

```bash
$ docker run -it --name keypass -v $(pwd):/home fednacl:latest
```

You may have now a new prompt

```bash
root@c26a753031ad /]# 
```

Move to `home` and make your build !

## When I'm done ?

```bash
root@c26a753031ad /]# exit
```

```bash
$ docker ps -a
CONTAINER ID        IMAGE                                 COMMAND                  CREATED              STATUS                        PORTS               NAMES
c26a753031ad        fednacl:latest                        "/bin/bash"              About a minute ago   Exited (127) 32 seconds ago                       keypass
```

```bash
$docker rm keypass
````
